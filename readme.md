# Gravities of the Solar System

Inspired by this excellent visualization: https://twitter.com/physicsJ/status/1414233142861262855

![Gif comparing the gravities of different bodies in the solar system](gravities.mp4)